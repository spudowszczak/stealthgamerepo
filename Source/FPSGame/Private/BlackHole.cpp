// Fill out your copyright notice in the Description page of Project Settings.

#include "BlackHole.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

#define OUT

// Sets default values
ABlackHole::ABlackHole()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DarkOrb = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlackOrb"));
	RootComponent = DarkOrb;
	DarkOrb->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	DarkOrb->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	
	InfluenceOrb = CreateDefaultSubobject<USphereComponent>(TEXT("InfluenceOrb"));
	InfluenceOrb->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	InfluenceOrb->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	InfluenceOrb->SetupAttachment(DarkOrb);
	InfluenceOrb->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
//	InfluenceOrb->SetSphereRadius(500.0f, true);

}

// Called when the game starts or when spawned
void ABlackHole::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlackHole::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Warning, TEXT("Founded: %s"), &InfluenceOrb->GetOverlappingComponents[0]);
	//UE_LOG(LogTemp, Warning, TEXT("AAA"));
	TArray<AActor*> OverlappedActors;
	InfluenceOrb->GetOverlappingActors(OUT OverlappedActors);
	
	for (AActor* Actor : OverlappedActors) {
		
		Cast<UStaticMeshComponent>(Actor);
		if (Actor) {
			UE_LOG(LogTemp, Warning, TEXT("aj %s"), *Actor->GetName());
			Actor->FindComponentByClass<UPrimitiveComponent>()->AddRadialForce(RootComponent->GetComponentLocation(), 40000.0f, -10000.0f, ERadialImpulseFalloff::RIF_Constant, true);
			//	Actor->FindComponentByClass<UPrimitiveComponent>()->AddForce()
			//	Actor->getCompo
		}
		//Actor->AddActorLocalTransform(FVector(1, 1, 1));
	}
	TArray<AActor*> SuckedActors;
	DarkOrb->GetOverlappingActors(OUT SuckedActors);
	for (AActor* Actor : SuckedActors) {
		Cast<UStaticMeshComponent>(Actor);
		if ((Actor)&&(Actor!=this)) {
			UE_LOG(LogTemp, Warning, TEXT("waj %s"), *Actor->GetName());
			Actor->Destroy();
		}
	}
}

void ABlackHole::NotifyActorBeginOverlap(AActor* OtherActor) {
	Super::NotifyActorBeginOverlap(OtherActor);
	//UE_LOG(LogTemp, Warning, TEXT("BBB"));
	//UE_LOG(LogTemp, Warning, TEXT("Founded: %s"), *OtherActor->GetName());
}

