// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSAIGuard.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "FPSGameMode.h"
#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"
#include "Navigation/PathFollowingComponent.h"
#include "Net/UnrealNetwork.h"






// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	GuardState = EAIState::Idle;
	EMoveState = Type::Failed;

	
}

// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();
	PawnSensComp->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnPawnSeen);
	PawnSensComp->OnHearNoise.AddDynamic(this,&AFPSAIGuard::OnHeardNoise);
	OriginalRotation = GetActorRotation();
	if (!bPatrolUnit) {
		return;
	}
	FindTargetPoints();
	APawn* tempPawn = Cast<APawn>(this);
	if (tempPawn) {
		UE_LOG(LogTemp, Warning, TEXT("Po�n"));
		tempAI = Cast<AAIController>(tempPawn->GetController());
		MoveToNextPosition();
		
	}
	//MoveToLocation(const FVector(0.0f, 0.0f, 0.0f),-1.0f,true,true,false);
	
}

void AFPSAIGuard::MoveToNextPosition() {
	if (tempAI) {
		UE_LOG(LogTemp, Warning, TEXT("EjAj"));
		//tempAI->MoveToLocation(FVector(0.0f,0.0f,0.0f));
		//EMoveState = tempAI->MoveToLocation(FVector(0.0f, 0.0f, 0.0f));
		EMoveState = tempAI->MoveToLocation(TargetsArray[CurrentTargetPoint]->GetActorLocation());

	}
}

void AFPSAIGuard::OnPawnSeen(APawn * SeenPawn)
{
	UE_LOG(LogTemp,Warning, TEXT("AJWAJ"));
		if (SeenPawn == nullptr) {
		return;
	}
		if (bPatrolUnit) {
			tempAI->PauseMove(0);
		}
	DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Green, false, 10.0f);
	AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if (GM) {
		GM->CompleteMission(SeenPawn, false);
	}
	SetAIGuardState(EAIState::Alerted);
}

void AFPSAIGuard::OnHeardNoise(APawn * NoiseInstigator, const FVector & Location, float Volume)
{
	if (GuardState == EAIState::Alerted) {
		return;
	}

	if (bPatrolUnit) {
		tempAI->PauseMove(0);
	}
	UE_LOG(LogTemp, Warning, TEXT("CICHAJ"));

	DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Red, false, 10.0f);

	FVector Direction = Location - GetActorLocation();
	Direction.Normalize();

	FRotator NewLookAt = FRotationMatrix::MakeFromX(Direction).Rotator();
	NewLookAt.Pitch = 0.0f;
	NewLookAt.Roll = 0.0f;
	SetActorRotation(NewLookAt);
	
		SetAIGuardState(EAIState::Suspicious);
	

	GetWorldTimerManager().ClearTimer(TimerHandle_ResetOrientation);
	GetWorldTimerManager().SetTimer(TimerHandle_ResetOrientation,this,&AFPSAIGuard::ResetOrientation,3.0f);
}

void AFPSAIGuard::OnRep_GuardState()
{
	OnStateChanged(GuardState);
}

void AFPSAIGuard::SetAIGuardState(EAIState NewState)
{
	if (GuardState == NewState) {
		return;
	}
	GuardState = NewState;
	OnRep_GuardState();
	OnStateChanged(GuardState);
		
}

void AFPSAIGuard::ResetOrientation()
{
	if (GuardState == EAIState::Alerted) {
		return;
	}

	if (bPatrolUnit) {
		tempAI->ResumeMove(0);
	}

	SetActorRotation(OriginalRotation	);
	SetAIGuardState(EAIState::Idle);
}

void AFPSAIGuard::FindTargetPoints()
{
	TSubclassOf<ATargetPoint> tempArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), TargetsArray);
	if(TargetsArray.Num() > 0) {
		UE_LOG(LogTemp, Warning, TEXT("TargetsArrayCoutn:%d"), TargetsArray.Num());
	}
}

// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bPatrolUnit) {
		return;
	}
//	if (EMoveState == EPathFollowingRequestResult::AlreadyAtGoal) {
	EPathStatus = tempAI->GetMoveStatus();
		UE_LOG(LogTemp, Warning, TEXT("%s Doszedl"),*GetName());
		if (EPathStatus == EPathFollowingStatus::Idle) {
			if (CurrentTargetPoint < TargetsArray.Num() - 1) {
				CurrentTargetPoint++;
			}
			else {
				CurrentTargetPoint = 0;
			}
			MoveToNextPosition();
		}

//	}
	/*if (uuu == EPathFollowingRequestResult::Type::RequestSuccessful) {
		UE_LOG(LogTemp, Warning, TEXT("Doszedl"));
	}*/
}

void AFPSAIGuard::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSAIGuard, GuardState);
}



