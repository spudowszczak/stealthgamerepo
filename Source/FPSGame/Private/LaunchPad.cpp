// Fill out your copyright notice in the Description page of Project Settings.

#include "LaunchPad.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/DecalComponent.h"
#include "FPSCharacter.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
//#include ""

class AFPSCharacter;
class AStaticMeshActor;
class UGameplayStatics;

// Sets default values
ALaunchPad::ALaunchPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"), true);
	LaunchPadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxMesh"),false);
	LaunchPadDecal = CreateDefaultSubobject<UDecalComponent>(TEXT("LaunchDecal"));
//	LaunchPadMesh->SetStaticMesh()
	RootComponent = LaunchPadMesh;
	LaunchPadMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	LaunchPadMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	LaunchPadMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	LaunchPadMesh->SetWorldScale3D(FVector(1.0f, 1.0f, 0.1f));
	

	OverlapComp->AttachTo(LaunchPadMesh);
	OverlapComp->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 100.0f), FRotator(0.0f, 0.0f, 0.0f));
	OverlapComp->SetBoxExtent(FVector(100.0f, 100.0f, 100.0f), true);
	OverlapComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	OverlapComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	OverlapComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Overlap);

	
	LaunchPadDecal->AttachTo(LaunchPadMesh);
	LaunchPadDecal->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 200.0f), FRotator(270.0f, 0.0f, 0.0f));
	LaunchPadDecal->SetRelativeScale3D(FVector(1.0f, 1.0f, 10.0f));
	LaunchPadDecal->DecalSize = FVector(100.0f, 100.0f, 100.0f);
	//LaunchPadDecal->DecalMaterial


	//OverlapComp->AttachToComponent(RootComponent);
	
	

}

// Called when the game starts or when spawned
void ALaunchPad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALaunchPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALaunchPad::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	AFPSCharacter* myFPSChar = Cast<AFPSCharacter>(OtherActor);
	if (myFPSChar) {
		UE_LOG(LogTemp, Warning, TEXT("DZUMP"));
		myFPSChar->LaunchCharacter(FVector(1000.0f,1000.0f,1000.0f), true, true);
		myFPSChar->LaunchCharacter((this->GetActorForwardVector()+ this->GetActorUpVector())*LaunchPower, true, true);
		UGameplayStatics::SpawnEmitterAtLocation(this, LaunchParticle, GetActorLocation());
		return;
	}
	AStaticMeshActor* myStaticMesh = Cast<AStaticMeshActor>(OtherActor);
	if (myStaticMesh) {
		UE_LOG(LogTemp, Warning, TEXT("DZUMPaa"));
		//myStaticMesh->GetStaticMeshComponent()->AddForce(FVector(100000.0f, 100000.0f, 100000.0f),NAME_None,true);
		//myStaticMesh->GetStaticMeshComponent()->AddForce((myStaticMesh->GetActorForwardVector()+myStaticMesh->GetActorUpVector())*100000.0f, NAME_None, true);
		myStaticMesh->GetStaticMeshComponent()->AddForce(((this->GetActorForwardVector() + this->GetActorUpVector())*(LaunchPower*100.0f)), NAME_None, true);
		return;
	}


}


