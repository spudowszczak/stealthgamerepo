// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Navigation/PathFollowingComponent.h"
#include "FPSAIGuard.generated.h"




UENUM(BlueprintType)
enum class EAIState :uint8 {
	Idle,
	Suspicious,
	Alerted
};



class UPawnSensingComponent;
class ATargetPoint;
class AAIController;
class UPathFollowingComponent;
//class EPathFollowingRequestResult;


using namespace  EPathFollowingRequestResult ;


UCLASS()
class FPSGAME_API AFPSAIGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSAIGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveToNextPosition();

	UPROPERTY(VisibleAnywhere,Category="Components")
	UPawnSensingComponent* PawnSensComp;

	UPROPERTY(EditInstanceOnly, Category = "AI")
		bool bPatrolUnit = false;

	UFUNCTION()
	void OnPawnSeen(APawn* SeenPawn);

	UFUNCTION()
		void OnHeardNoise(APawn* NoiseInstigator, const FVector& Location, float Volume);

	FRotator OriginalRotation;

	UPROPERTY(ReplicatedUsing=OnRep_GuardState)
	EAIState GuardState;
	
	UFUNCTION()
	void OnRep_GuardState();

	int8 CurrentTargetPoint = 0;

	AAIController* tempAI=nullptr;
	//UPathFollowingComponent*::EPathFollowingRequestResult::Type;
	
	EPathFollowingRequestResult::Type EMoveState;
	EPathFollowingStatus::Type EPathStatus;

	
	
	void SetAIGuardState(EAIState NewState);

	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnStateChanged(EAIState NewState);

	UFUNCTION()
	void ResetOrientation();

	TArray<AActor*> TargetsArray;

	void FindTargetPoints();

	FTimerHandle TimerHandle_ResetOrientation;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	
	
};
