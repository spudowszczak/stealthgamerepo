// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LaunchPad.generated.h"

class UBoxComponent;
class UStaticMeshComponent;
class UDecalComponent;

UCLASS()
class FPSGAME_API ALaunchPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALaunchPad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UBoxComponent* OverlapComp;
	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* LaunchPadMesh;
	UPROPERTY(VisibleAnywhere, Category = "Components")
		UDecalComponent* LaunchPadDecal;
	UPROPERTY(EditAnywhere, Category = "Force", meta = (ClampMin = 0.0f, ClampMax = 10000.0f, UIMin = 0.0f, UIMax = 10000.0f))
		float LaunchPower = 1000.0f;
	UPROPERTY(EditDefaultsOnly, Category = "LaunchPad")
		UParticleSystem* LaunchParticle;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor);
	
	
};
